const days_of_week = [
  'Sun',
  'Mon',
  'Tue',
  'Wed',
  'Thu',
  'Fri',
  'Sat',
];

const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

let date = new Date();
let day = date.getDate();
let month = date.getMonth();
let year = date.getFullYear();

let selectedDate = date;
let selectedDay = day;
let selectedMonth = month;
let selectedYear = year;

const globalCssClass = 'cs-datepicker';

function appendMultipleChildren(children, parent) {
  return children.map((child) => {
    parent.appendChild(child);
  });
}

function getDateParam(date, paramType) {
  let d = new Date(date);
  switch (paramType) {
    case 'month':
      return d.getMonth()
    case 'year':
      return d.getFullYear()
    case 'date':
      return d.getDate()
    default:
      return d
  }
}

const chevronLeft = `<svg aria-hidden="true" width="13px" focusable="false" data-prefix="fas" data-icon="chevron-left" class="svg-inline--fa fa-chevron-left fa-w-10" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path fill="#313131" d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z"></path></svg>`;

const chevronRight = `<svg aria-hidden="true" width="13px" focusable="false" data-prefix="fas" data-icon="chevron-right" class="svg-inline--fa fa-chevron-right fa-w-10" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path fill="#313131" d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"></path></svg>`;

const elements = {
  datesEl: crtEl('div', `${globalCssClass}--dates`),
  mthContEl: crtEl('div', `${globalCssClass}--month-container`),
  prevBtn: crtEl('button', `${globalCssClass}--prev-month arrows`),
  nextBtn: crtEl('button', `${globalCssClass}--next-month arrows`),
  mthTxt: crtEl('div', `${globalCssClass}-month-text`),

  table: crtEl('table', `${globalCssClass}--table`),
  thead: crtEl('thead', `${globalCssClass}--thead`),
  tbody: crtEl('tbody', `${globalCssClass}--tbody`),
  body: document.querySelector('body'),
  activeDateElement: document.querySelector('.datepicker-active'),
  day: document.querySelector(`${globalCssClass}--td`),
  closeBtn: crtEl('button', `${globalCssClass}--close-btn`)
}

function crtEl(component = "div", classname = '', addClass = false) {
  const el = document.createElement(component);
  if (addClass) {
    el.classList.add(classname);
  } else {
    el.className = classname;
  }
  return el;
}

function formatDate(d) {
  day = d.getDate() < 10 ? `0${d.getDate()}` : d.getDate();
  let month = d.getMonth() + 1;
  month = month < 10 ? `0${month}` : month;
  let year = d.getFullYear();
  return `${day}-${month}-${year}`;
}

function renderWeekDays() {
  let row = crtEl('tr');
  days_of_week.map((day) => {
    let td = crtEl('td', `${globalCssClass}--td`);
    td.textContent = day;
    row.appendChild(td);
  });
  elements.thead.appendChild(row);
  elements.datesEl.appendChild(elements.table);
}

function renderDates(month, year, slots = [], el) {
  let firstDay = new Date(year, month).getDay();
  let daysInMonth = 32 - new Date(year, month, 32).getDate();
  let tbl = document.querySelector(`.${globalCssClass}--tbody`);

  tbl.innerHTML = '';

  let date = 1;
  for (let i = 0; i < 6; i++) {
    let row = crtEl('tr');
    for (let j = 0; j < 7; j++) {
      if (i === 0 && j < firstDay) {
        let cell = crtEl('td', `${globalCssClass}--td`);
        cell.textContent = '';
        row.appendChild(cell);
      } else if (date > daysInMonth) {
        break;
      } else {
        let cell = crtEl('td', `${globalCssClass}--td`);
        let slotEl = crtEl('span', 'slots', true);
        cell.textContent = `${date}`;
        cell.setAttribute("data-date", `${date}`);
        if (slots && slots.length > 0) {
          cell.classList.add('disabled')
          slots.map((slot) => {
            if (getDateParam(slot.date, 'date') == date && getDateParam(slot.date, 'month') == month && getDateParam(slot.date, 'year') == year) {
              slotEl.textContent = slot.noOfSlot;
              cell.appendChild(slotEl);
              cell.classList.remove('disabled');
              cell.addEventListener('click', () => {
                selectedDate = new Date(year, month, getDateParam(slot.date, 'date'))
                el.value = formatDate(selectedDate)
                el.dataset.date = `${year}/${month}/${getDateParam(slot.date, 'date')}`;
                hide();
              })
            }
          })
        } else {
          cell.addEventListener('click', () => {
            selectedDate = new Date(year, month, cell.dataset.date)
            el.value = formatDate(selectedDate)
            el.dataset.date = selectedDate;
            hide();
          })
        }
        row.appendChild(cell);
        date++;
      }
    }

    tbl.appendChild(row);
  }
}

function next(el) {
  let pickerEl = elements.datesEl.previousElementSibling;
  let slots = JSON.parse(el.dataset.slots);
  selectedYear = (selectedMonth === 11) ? selectedYear + 1 : selectedYear;
  selectedMonth = (selectedMonth + 1) % 12;
  elements.mthTxt.textContent = `${months[selectedMonth]} ${selectedYear}`
  renderDates(selectedMonth, selectedYear, slots, pickerEl);
}

function previous(el) {
  let pickerEl = elements.datesEl.previousElementSibling;
  let slots = JSON.parse(el.dataset.slots);
  selectedYear = (selectedMonth === 0) ? selectedYear - 1 : selectedYear;
  selectedMonth = (selectedMonth === 0) ? 11 : selectedMonth - 1;
  elements.mthTxt.textContent = `${months[selectedMonth]} ${selectedYear}`
  renderDates(selectedMonth, selectedYear, slots, pickerEl);
}

function render(element) {
  const mthChildren = [elements.prevBtn, elements.mthTxt, elements.nextBtn];
  const tblCh = [elements.thead, elements.tbody];
  // elements.prevBtn.innerHTML = chevronLeft;
  // elements.nextBtn.innerHTML = chevronRight;
  elements.closeBtn.innerHTML = 'X';
  elements.mthTxt.textContent = `${months[month]} ${year}`;
  appendMultipleChildren(mthChildren, elements.mthContEl);
  appendMultipleChildren(tblCh, elements.table);
  appendMultipleChildren([elements.closeBtn, elements.mthContEl, elements.table], elements.datesEl);
  element.after(elements.datesEl);
}


function show(el) {
  let date = new Date(el.dataset.date);
  let month = date.getMonth();
  let year = date.getFullYear();

  let slots = el.dataset.slots ? JSON.parse(el.dataset.slots) : [];
  elements.prevBtn.dataset.slots = JSON.stringify(slots);
  elements.nextBtn.dataset.slots = JSON.stringify(slots);
  render(el)
  renderDates(month, year, slots, el)
  elements.datesEl.classList.add('show');
  responsive(el)
  onResizeWindow(el)
}

function hide(el) {
  let dateContainer = document.querySelector('.cs-datepicker--dates');
  let dateParentElement = dateContainer.parentElement
  if (dateContainer) {
    dateContainer.classList.remove('show');
    dateParentElement.removeChild(dateContainer);
  }
}

function onResizeWindow(el) {
  window.addEventListener('resize', () => responsive(el))

}

function responsive(el) {
  let inputPosition = el.offsetLeft;
  let left = '';
  if (inputPosition < 300) {
    left = 2;
  } else if (inputPosition >= 300 && inputPosition < 461) {
    left = 20;
  } else if (inputPosition > 460) {
    left = 50;
  }
  elements.datesEl.style.left = `${left}%`
}

let inputsElements = document.querySelectorAll('.cs-datepicker');
Array.prototype.forEach.call(inputsElements, function (input) {
  input.value = formatDate(selectedDate);
  input.dataset.date = selectedDate;
  input.addEventListener('click', (e) => show(e.target));
});

elements.prevBtn.addEventListener('click', (e) => previous(e.target))
elements.nextBtn.addEventListener('click', (e) => next(e.target))
elements.closeBtn.addEventListener('click', () => hide())
renderWeekDays();
