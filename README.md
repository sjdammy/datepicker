# README

# Custom date picker

### Features

- Regular date
- Pick dates based on available slots. (When slots are provided)

### Usage

Add the stylesheet to the head tag and add the script tag before the closing body tag.

Give an input field an id

`<input type="text" id="myDatepicker">`

The datepicker function accepts two parameters

- input id
- available slots (optional) - Array[Object]

  Pass the id of the input element and available slots

  `<script> let slots = [{ date: 15, month: 4, year: 2020, noOfSlot: 20, }, { date: 12, month: 4, year: 2020, noOfSlot: 40, }, { date: 15, month: 5, year: 2020, noOfSlot: 40, },]; Datepicker('myDatepicker', slots); </script>`
